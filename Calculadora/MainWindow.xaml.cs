﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using System.Drawing;

namespace Calculadora
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Operaciones> oper = new List<Operaciones>();
        Operadores inversor = new Operadores('+', 3);
        Operadores potencia = new Operadores('^', 3);
        Operadores raiz = new Operadores('$', 3);
        public MainWindow()
        {
            InitializeComponent();


        }

        private void btn0_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "0";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "0";
            }

        }

        private void btn1_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "1";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "1";
            }
        }
        private void btn2_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "2";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "2";
            }
        }

        private void btn3_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "3";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "3";
            }
        }

        private void btn4_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "4";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "4";
            }
        }

        private void btn5_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "5";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "5";
            }
        }

        private void btn6_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "6";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "6";
            }
        }

        private void btn7_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "7";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "7";
            }
        }

        private void btn8_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "8";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "8";
            }
        }

        private void btn9_agregar(object sender, RoutedEventArgs e)
        {
            if (contieneCero())
            {
                txtLowerDisplay.Text = "9";
            }
            else if (caracteresDisponibles())
            {
                txtLowerDisplay.Text += "9";
            }
        }

        private void btnClear_click(object sender, RoutedEventArgs e)
        {
            txtLowerDisplay.Text = "";
            txtHigherDisplay.Text = "";
            oper.Clear();

        }

        /////////////////////////////////operadores especiales////////////////////////////////////////////////////////////////
        private void txtLowerDisplay_txtChange(object sender, TextChangedEventArgs e)
        {
            if (txtLowerDisplay.Text.Length == 0)
            {
                txtLowerDisplay.Text = "0";
            }
        }



        private void BtnDivision_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            txtHigherDisplay.Text += txtLowerDisplay.Text + " / ";
            float val = float.Parse(txtLowerDisplay.Text);
            agregarOperacion(new Operadores('/', 1), val);
            txtLowerDisplay.Text = "0";
            txtHigherDisplay.Select(txtHigherDisplay.Text.Length, 0);
            txtHigherDisplay.CaretIndex = txtHigherDisplay.Text.Length;
            txtHigherDisplay.ScrollToEnd();
        }

        private void BtnMultiplier_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            txtHigherDisplay.Text += txtLowerDisplay.Text + " * ";
            float val = float.Parse(txtLowerDisplay.Text);
            agregarOperacion(new Operadores('*', 1), val);
            txtLowerDisplay.Text = "0";
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            if (contieneCero()) txtLowerDisplay.Text = "-";
            else {
                if (negativoSinNumero())
                {
                    txtLowerDisplay.Text += "0";
                }
                    clearAfterResult();
                    txtHigherDisplay.Text += txtLowerDisplay.Text + " - ";
                    float val = float.Parse(txtLowerDisplay.Text);
                    agregarOperacion(new Operadores('-', 2), val);
                    txtLowerDisplay.Text = "0";
                 

            }

        }

        private void BtnPlus_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            txtHigherDisplay.Text += txtLowerDisplay.Text + " + ";
            float val = float.Parse(txtLowerDisplay.Text);
            agregarOperacion(new Operadores('+', 2), val);
            txtLowerDisplay.Text = "0";
            txtHigherDisplay.Select(txtHigherDisplay.Text.Length, 0);
            txtHigherDisplay.CaretIndex = txtHigherDisplay.Text.Length;
            txtHigherDisplay.ScrollToEnd();

        }

        private void BtnEquals_Click(object sender, RoutedEventArgs e)
        {
            if (!contieneResultado())
            {

                txtHigherDisplay.Text += txtLowerDisplay.Text + " = ";
                float val = float.Parse(txtLowerDisplay.Text);
                int last = oper.Count - 1;
                oper[last].setValor2(val);
                foreach (Operaciones op in oper)
                {
                    Console.WriteLine(" operacion " + op.getValor1() + "  " + op.getOperador().getSimbolo() + "  " + op.getValor2());
                }
                txtLowerDisplay.Text = resultado();
                oper.Clear();
                txtHigherDisplay.Select(txtHigherDisplay.Text.Length, 0);
                txtHigherDisplay.CaretIndex = txtHigherDisplay.Text.Length;
                txtHigherDisplay.ScrollToEnd();

            }


        }


        private void BtnDot_Click(object sender, RoutedEventArgs e)
        {
            String text = txtLowerDisplay.Text;
            if (!text.Contains("."))
            {
                if (contieneCero()) txtLowerDisplay.Text = "0.";
                else txtLowerDisplay.Text += ".";
            }
            else;


        }
        private void BtnElevate_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            float val = float.Parse(txtLowerDisplay.Text);
            txtHigherDisplay.Text = txtLowerDisplay.Text + "^2=" + (val * val);
            txtHigherDisplay.CaretIndex = txtHigherDisplay.Text.Length;
            txtLowerDisplay.Text = (val * val).ToString();
        }

        private void BtnSquare_Click(object sender, RoutedEventArgs e)
        {
            clearAfterResult();
            float val = float.Parse(txtLowerDisplay.Text);
            Double res = Math.Sqrt(val);
            txtHigherDisplay.Text = "√" + txtLowerDisplay.Text + "=" + res;
            if (!Double.IsNaN(res)) txtLowerDisplay.Text = res.ToString();
            else txtLowerDisplay.Text = "Error";

        }

        private void BtnInvert_Click(object sender, RoutedEventArgs e)
        {
            float val = float.Parse(txtLowerDisplay.Text);
            float res = (val * (-1));
            txtHigherDisplay.Text = txtLowerDisplay.Text + "(-x)=" + res;
            txtLowerDisplay.Text = res.ToString();
        }


        ///////////////////////////////////////////Funciones////////////////////////////////////////////////////

        private Boolean caracteresDisponibles()
        {
            return (txtLowerDisplay.Text.Length < 10);

        }
        private Boolean contieneCero()
        {
            return (txtLowerDisplay.Text.Length == 1 && txtLowerDisplay.Text.Equals("0"));

        }

        public Boolean contieneResultado()
        {
            return txtHigherDisplay.Text.Contains("=");
        }

        public void clearAfterResult()
        {
            if (contieneResultado()) txtHigherDisplay.Text = "";
        }


        private void agregarOperacion(Operadores op, float val1)
        {
            if (oper.Count == 0)
            {
                Operaciones operacion = new Operaciones(op);
                operacion.setValor1(val1);
                oper.Add(operacion);

            }
            else
            {
                Operaciones operacion = new Operaciones(op);
                int last = oper.Count - 1;
                oper[last].setValor2(val1);
                operacion.setValor1(val1);
                oper.Add(operacion);

            }

        }

        public int mejorOperacionAnterior(int current)
        {
            for (int i = current-1; i >= 0; i--)
            {
                if (oper[i].getStatus() == 1) return i;
            }
            return 0;
        }

        public int mejorOperacionSiguiente(int current)
        {
            for (int i = current+1; i < oper.Count; i++)
            {
                Console.WriteLine("mejor operacion siguiente: indice  " + i + " status " + oper[i].getStatus());
                if (oper[i].getStatus() == 1) return i;

            }
            return 0;
        }

        public Boolean mejorOperacion(int anterior, int siguiente)
        {
            if (oper[anterior].getOperador().getPrioridad() <= oper[siguiente].getOperador().getPrioridad()) return oper[anterior].getStatus() == 1;
            else
            {
                if (oper[anterior].getStatus() == 1) return false;
                else return true;
            }

        }

        public String resultado()
        {
            float res = 0;

            if (oper.Count == 1)
            {
                return ((float)Math.Round(realizarOperacion(oper[0]) * 100f) / 100f).ToString();
            }
            else {
                for (int i = 1; i < 3; i++)
                {
                    for (int k = 0; k < oper.Count; k++)
                    {

                        if (oper[k].getStatus() == 1 && oper[k].getOperador().getPrioridad() == i)
                        {
                            Console.WriteLine("Se esta resolviendo la operacion  " + k);
                            res = realizarOperacion(oper[k]);
                            if (k == 0)
                            {
                                
                                oper[mejorOperacionSiguiente(k)].setValor1(res);
                                Console.WriteLine("aqui es el indice 0, este es la mejor siguiente:  " + mejorOperacionSiguiente(k));
                                
                            }
                            else
                            {

                                
                                

                                if (mejorOperacion(mejorOperacionAnterior(k), mejorOperacionSiguiente(k)))
                                {
                                    oper[mejorOperacionAnterior(k)].setValor2(res);
                                    if (k + 1 == oper.Count - 1) oper[k + 1].setValor1(res);
                                } else oper[mejorOperacionSiguiente(k)].setValor1(res);

                               // Console.WriteLine("la mejor operacion " + mejorOperacionAnterior(k) +"  anterior status"+oper[mejorOperacionAnterior(k)].getStatus()+" " +mejorOperacionSiguiente(k)+"  siguiente  status"+mejorOperacionSiguiente
                                 //   (k)+"   "+mejorOperacion(mejorOperacionAnterior(k), mejorOperacionSiguiente(k)));

                            }
                            oper[k].setStatus(0);
                        }

                        /*foreach (Operaciones op in oper)
                        {
                            if (op.getStatus() == 1) Console.WriteLine("ciclo " + k + " prioridad " + i + " status "+ op.getOperador().getPrioridad()+" "+op.getStatus()+" operacion " + op.getValor1() + "  " + op.getOperador().getSimbolo() + "  " + op.getValor2());
                        }*/
                    }

                }

                return ((float)Math.Round(res * 100f) / 100f).ToString();
            }

        }


        public float realizarOperacion(Operaciones op)
        {
            float res = 0;
            switch (op.getOperador().getSimbolo())
            {
                case '*':
                    res = op.getValor1() * op.getValor2();
                    for (int i = 0; i < oper.Count; i++)
                    {
                        if (oper[i].getStatus() == 1) Console.WriteLine("ïndex " + i + oper[i].getOperador().getSimbolo() + "  " + oper[i].getValor1() + "  " + oper[i].getValor2() + "  " + (res));
                    }




                    break;
                case '/':
                    res = op.getValor1() / op.getValor2();
                    for (int i = 0; i < oper.Count; i++)
                    {
                        if (oper[i].getStatus() == 1) Console.WriteLine("ïndex " + i + oper[i].getOperador().getSimbolo() + "  " + oper[i].getValor1() + "  " + oper[i].getValor2() + "  " + (res));
                    }


                    break;
                case '+':
                    res = op.getValor1() + op.getValor2();
                    for (int i = 0; i < oper.Count; i++)
                    {
                        if (oper[i].getStatus() == 1) Console.WriteLine("ïndex " + i + oper[i].getOperador().getSimbolo() + "  " + oper[i].getValor1() + "  " + oper[i].getValor2() + "  " + (res));
                    }


                    break;
                case '-':
                    res = op.getValor1() - op.getValor2();
                    for (int i = 0; i < oper.Count; i++)
                    {
                        if (oper[i].getStatus() == 1) Console.WriteLine("ïndex " + i + oper[i].getOperador().getSimbolo() + "  " + oper[i].getValor1() + "  " + oper[i].getValor2() + "  " + (res));
                    }
                    break;
            }
            return res;
        }

        private void TxtHigherDisplay_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtHigherDisplay.Select(txtHigherDisplay.Text.Length, 0);
        } 

        public Boolean negativoSinNumero()
        {
            return txtLowerDisplay.Text.Length == 1 && txtLowerDisplay.Text == "-";
            
        }
    }
}
