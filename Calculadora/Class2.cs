﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora
{
    public class Operadores
    {
        private char simbolo;
        private int prioridad;
        public Operadores(char simbolo, int prioridad)
        {
            this.simbolo = simbolo;
            this.prioridad = prioridad;
            
        }

        public int getPrioridad()
        {
            return prioridad;
        
        }

        public char getSimbolo()
        {
            return simbolo;
        }

    }


    public class Operaciones{
        private float valor1, valor2;
        private int status = 1;
        Operadores op;

        public Operaciones(Operadores op)
        {
            this.op = op;
        }

        public void setOperador(Operadores op)
        {
            this.op = op;
        }
        public void setValor1(float valor1)
        {
            this.valor1 = valor1;
        }
        public void setStatus(int status)
        {
            this.status= status;
        }
        public void setValor2(float valor2)
        {
            this.valor2 = valor2;
        }

        public Operadores getOperador()
        {
            return op;
        }

        public float getValor1()
        {
            return valor1;
        }
        public float getValor2()
        {
            return valor2 ;
        }

        public int getStatus()
        {
            return status;
        }



    }
    
}
